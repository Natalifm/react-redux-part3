import authRoutes from './authRoutes';
import userRoutes from './userRoutes';
import postRoutes from './postRoutes';

export default (app) => {
    app.use('/api/auth', authRoutes);
    app.use('/api/posts', postRoutes);
    app.use('/api/users', userRoutes);
};