import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {store} from './redux/store';
import {history} from './redux/middleware';
import './index.css';
import ChatApp from './navigation/ChatApp';

render(
    <Provider store={store}>
        <BrowserRouter history={history}>
            <ChatApp/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
