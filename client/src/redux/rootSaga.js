import {all, call} from 'redux-saga/effects';
import {watchAuth} from '../sagas/auth/saga/watchers';
import {watchUsers} from '../sagas/users/saga/watchers';
import {watchMessages} from '../sagas/messages/saga/watchers';

export function* rootSaga() {
    yield all([call(watchAuth), call(watchMessages), call(watchUsers)]);
}