import {combineReducers} from 'redux';
import {uiReducer as ui} from '../sagas/ui/reducer';
import {authReducer as auth} from '../sagas/auth/reducer';
import {messagesReducer as messages} from '../sagas/messages/reducer';
import {userReducer as users} from '../sagas/users/reducer';

export const rootReducer = combineReducers({
    ui,
    auth,
    messages,
    users
});