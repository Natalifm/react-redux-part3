import React, {useState} from 'react';
import {connect} from 'react-redux';
import {bool, func} from 'prop-types';
import Spinner from '../Spinner/Spinner';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {authActions} from '../../sagas/auth/actions';
import './Login.css';

const mapStateToProps = (state) => ({
    isFetching: state.ui.isFetching,
});

const mapDispatchToProps = {
    loginAsync: authActions.loginAsync,
};

const Login = (props) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        props.loginAsync({login, password});
    }

    return (
        <>
            <Spinner/>
            <form
                className='login_form'
                onSubmit={handleSubmit}>
                <TextField
                    label="Enter user name..."
                    type="text"
                    variant="filled"
                    value={login}
                    onChange={(e) => setLogin(e.target.value)}
                    margin="normal"
                    required
                />
                <TextField
                    label="password"
                    type="password"
                    variant="filled"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    margin="normal"
                    required
                />
                <Button type="submit" variant="contained" color="secondary">
                    Login
                </Button>
            </form>
        </>
    )
}

Login.propTypes = {
    isFetching: bool.isRequired,
    loginAsync: func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
