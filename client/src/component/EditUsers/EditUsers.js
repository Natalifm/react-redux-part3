import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import uuid from 'react-uuid'
import {array} from 'prop-types';
import {userActions} from '../../sagas/users/actions';

import './UserEditor.css';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
    },
}));

const EditUsers = (props) => {
    const classes = useStyles();
    const [newName, setNewName] = useState('');
    const [newEmail, setNewEmail] = useState('');
    const [newPassword, setNewPassword] = useState('');

    const {users, editingUserId, history, actions} = props;
    const editingUser = users.find(user => user.id === editingUserId);

    useEffect(() => {
        if (editingUser) {
            setNewName(editingUser.name);
            setNewEmail(editingUser.email);
            setNewPassword(editingUser.password);
        }
    }, [editingUser]);

    const clearAll = () => {
        actions.clearEditingUserId();
        setNewName('');
        setNewEmail('');
        setNewPassword('');
    }

    const createOrUpdateUser = (e) => {
        e.preventDefault();
        editingUserId
            ? actions.updateUserAsync({
                id: editingUserId,
                name: newName,
                email: newEmail,
                password: newPassword,
                role: 'user'
            })
            : actions.createUserAsync({
                id: uuid(),
                name: newName,
                email: newEmail,
                password: newPassword,
                role: 'user'
            })
        clearAll();
        history.push('/user_list');
    }

    const cancelUpdateMessage = () => {
        clearAll();
        history.push('/user_list');
    }

    return (
        <Container maxWidth="sm">
            <form onSubmit={createOrUpdateUser}>
                <TextField
                    label="Enter usename..."
                    type="text"
                    value={newName}
                    onChange={(e) => setNewName(e.target.value)}
                    margin="normal"
                    required
                    variant="filled"
                />
                <TextField
                    label="email"
                    type="email"
                    value={newEmail}
                    onChange={(e) => setNewEmail(e.target.value)}
                    margin="normal"
                    required
                    variant="filled"
                />
                <TextField
                    label="password"
                    type="password"
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    margin="normal"
                    required
                    variant="filled"
                />
                <Button
                    type="submit"
                    color="secondary"
                    variant="outlined"
                    className={classes.button}>
                    {editingUser ? 'Update' : 'Create'}
                </Button>
                <Button
                    onClick={cancelUpdateMessage}
                    color="default"
                    variant="outlined"
                    className={classes.button}
                    autoFocus>Cancel</Button>
            </form>
        </Container>
    )
}

const mapStateToProps = (state) => ({
    users: state.users.userList,
    editingUserId: state.users.editingUserId,
})

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            ...userActions
        }, dispatch),
    }
}

EditUsers.propTypes = {
    users: array.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUsers);
