import React from 'react';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import {withStyles} from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import {Delete, FavoriteBorder, Settings} from '@material-ui/icons';

import {func, shape, bool, string} from 'prop-types';
import './Message.css';

const Message = ({post, history, toggleMessageLike, deleteMessageAsync, setEditingMessageId}) => {
    const {id, avatar, createdAt, message, user, isLiked} = post;

    const favoriteBorderColor = isLiked ? '#f00' : '#000';
    const myMessage = user === 'me' ? 'auto' : '0';

    const handleEditMessage = () => {
        setEditingMessageId(id);
        history.push('/edit_message');
    }

    const createDeleteIcon = () => user === 'me'
        ? <Delete
            className='delete'
            onClick={() => deleteMessageAsync(id)}/>
        : null;
    const favoriteBorderIcon = () => user !== 'me'
        ? <FavoriteBorder
            className='like'
            style={{color: favoriteBorderColor}}
            onClick={() => toggleMessageLike(id)}/>
        : null;
    const card = user === 'me'
        ? <IconTooltip
            placement="left"
            interactive={true}
            title={
                <Settings
                    onClick={handleEditMessage}
                />
            }>
            <Card
                className='card'
                style={{marginLeft: myMessage}}>
                {avatar && <Avatar src={avatar} alt="user" style={{margin: 10}}/>}
                <div style={{width: '80%', maxWidth: '100%'}}>
                    <p>{createdAt}</p>
                    <p>{message}</p>
                </div>
                {createDeleteIcon()}
                {favoriteBorderIcon()}
            </Card>
        </IconTooltip>
        : <Card
            className='card'
            style={{marginLeft: myMessage}}>
            {avatar && <Avatar src={avatar} alt="user" style={{margin: 10}}/>}
            <div style={{width: '80%', maxWidth: '100%'}}>
                <p>{createdAt}</p>
                <p>{message}</p>
            </div>
            {createDeleteIcon()}
            {favoriteBorderIcon()}
        </Card>
    return card;
}

const IconTooltip = withStyles(theme => ({
    tooltip: {
        backgroundColor: '#e75677',
        color: 'rgba(80,75,75,0.87)',
        maxWidth: 220,
        border: '1px solid #dadde9',
    },
}))(Tooltip);

Message.propTypes = {
    post: shape({
        id: string.isRequired,
        avatar: string,
        createdAt: string.isRequired,
        message: string.isRequired,
        user: string.isRequired,
        isLiked: bool
    }),
    toggleMessageLike: func.isRequired,
    deleteMessageAsync: func.isRequired,
    setEditingMessageId: func.isRequired,
}

export default Message;
