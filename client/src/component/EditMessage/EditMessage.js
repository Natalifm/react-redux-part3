import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Button from '@material-ui/core/Button';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import {array} from 'prop-types';
import {messagesActions} from '../../sagas/messages/actions';
import DialogTitle from "@material-ui/core/DialogTitle";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import './EditMessage.css';

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
    },
}));

const EditMessage = (props) => {
    const classes = useStyles();
    const {messages, editingMessageId, history, actions} = props;
    const [editingText, setEditingText] = useState('');
    const editingMessage = messages.find(message => message.id === editingMessageId);

    useEffect(() => {
        setEditingText(editingMessage.message);
    }, [editingMessage.message]);

    const saveMessage = () => {
        actions.updateMessageAsync(editingMessage.id, editingText);
        history.replace('/chat')
    }

    return (
        <Container maxWidth="md">
            <DialogTitle className="edit-title">{"Edit message"}</DialogTitle>
            <TextareaAutosize
                className='edit-message-textarea'
                aria-label="Textarea"
                rows={6}
                value={editingText}
                onChange={e => setEditingText(e.target.value)}/>
            <Button onClick={saveMessage} color="secondary" variant="contained" className={classes.button}>
                OK
            </Button>
            <Button onClick={() => {
                history.replace('/chat')
            }} color="default" variant="contained" className={classes.button}>
                Cancel
            </Button>
        </Container>
    );
}

EditMessage.propTypes = {
    messages: array.isRequired,
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messageList,
        editingMessageId: state.messages.editingMessageId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({
            ...messagesActions
        }, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);