import React, {useState} from 'react';
import uuid from 'react-uuid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {func} from 'prop-types';
import './TextInput.css';

const TextInput = (props) => {
    const [message, setMessage] = useState('');

    const handleCreateMessage = (e) => {
        const createdAt = new Date().toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'})
        e.preventDefault();
        if (!(message.trim())) {
            return;
        }
        props.createMessageAsync({
            id: uuid(),
            user: "me",
            createdAt,
            isLiked: false,
            message,
        });
        setMessage('');
    }

    return (
        <>
            <form
                onSubmit={handleCreateMessage}
                className='form'>
                <TextField
                    label="Write a message..."
                    className="textInput"
                    variant="filled"
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                />
                <Button
                    className="inputBtn"
                    variant="contained"
                    color="secondary"
                    size="small"
                    type="submit">Send</Button>
            </form>
        </>
    )
}

TextInput.propTypes = {
    createMessageAsync: func.isRequired
}

export default TextInput;
