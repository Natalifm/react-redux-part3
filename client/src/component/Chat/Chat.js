import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import {array, string} from 'prop-types';

import Header from "../Header/Header";
import UsersMessages from "../UsersMessages/UsersMessages";
import TextInput from "../TextInput/TextInput";
import Spinner from '../Spinner/Spinner';

import {messagesActions} from '../../sagas/messages/actions';
import Logo from "../Logo/Logo";
import Footer from "../Footer/Footer";

class Chat extends Component {
    componentDidMount() {
        const {actions} = this.props;
        actions.fetchMessages();
    }

    static propTypes = {
        messages: array.isRequired,
        editingMessageId: string.isRequired
    }

    render() {
        const {messages, history, actions} = this.props;
        const participants = (new Set(messages.map(({user}) => user))).size;
        const lastMessageTime = messages.length && messages[messages.length - 1].createdAt;

        return (
            <>
                {messages.length
                    ? <>
                        <Spinner/>
                        <Logo/>
                        <Header
                            usersmessages={messages.length}
                            participants={participants}
                            lastMessageTime={lastMessageTime}
                        />
                        <CssBaseline/>
                        <Container maxWidth="md">
                            <UsersMessages
                                messages={messages}
                                history={history}
                                toggleMessageLike={actions.toggleMessageLike}
                                deleteMessageAsync={actions.deleteMessageAsync}
                                setEditingMessageId={actions.setEditingMessageId}
                            />
                            <TextInput
                                createMessageAsync={actions.createMessageAsync}
                            />
                        </Container>
                        <Footer/>
                    </>
                    : null
                }
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messageList,
        editingMessageId: state.messages.editingMessageId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(messagesActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
