import React, {Fragment} from 'react';
import Divider from '@material-ui/core/Divider';
import Message from "../Message/Message";
import {array, func} from 'prop-types';

const UsersMessages = ({messages, history, deleteMessageAsync, toggleMessageLike, setEditingMessageId}) => {
    const createMessageList = () => {
        return messages.map((post, index) => {
            const card = <Message
                key={post.id}
                post={post}
                history={history}
                toggleMessageLike={toggleMessageLike}
                deleteMessageAsync={deleteMessageAsync}
                setEditingMessageId={setEditingMessageId}/>;

            const nextMessage = messages[index + 1];
            const nextDay = nextMessage && new Date(messages[index + 1].createdAt);
            const previousDay = new Date(messages[index].createdAt);

            if (nextMessage && nextDay.getDate() - previousDay.getDate()) {

                return (
                    <Fragment key={post.id}>
                        {card}
                        <Divider style={{marginTop: 20}}/>
                        <p style={{textAlign: 'center'}}>
                            {(new Date(messages[index + 1].createdAt))
                                .toLocaleString('en', {
                                    month: 'long',
                                    day: 'numeric'
                                })}</p>
                    </Fragment>
                )
            } else {
                return card;
            }
        })
    }
    const messageList = createMessageList();
    return (
        <>
            {messageList}
        </>
    )
}

UsersMessages.propTypes = {
    messages: array.isRequired,
    toggleMessageLike: func.isRequired,
    deleteMessageAsync: func.isRequired,
    setEditingMessageId: func.isRequired,
}

export default UsersMessages;
