import React from 'react';
import {connect} from 'react-redux';
import {Admin} from './Admin';
import {Public} from './Public';

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    isAdmin: state.auth.isAdmin,
});

const ChatApp = props => {
    const {isAuthenticated, isAdmin} = props;
    return isAuthenticated ? <Admin isAdmin={isAdmin}/> : <Public/>;
}

export default connect(mapStateToProps)(ChatApp);
