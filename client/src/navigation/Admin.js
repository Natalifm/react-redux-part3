import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import EditMessage from '../component/EditMessage/EditMessage';
import UserList from '../component/UsersList/UserList';
import UserEditor from '../component/EditUsers/EditUsers';
import Chat from '../component/Chat/Chat';

const Admin = props => {
    const {isAdmin} = props;
    return (
        <Switch>
            <Route exact component={Chat} path={'/chat'}/>
            <Route exact component={EditMessage} path={'/edit_message'}/>
            {isAdmin && <Route exact component={UserEditor} path={'/user_editor'}/>}
            {isAdmin && <Route exact component={UserList} path={'/user_list'}/>}
            <Redirect to={isAdmin ? '/user_list' : '/chat'}/>
        </Switch>
    );
}

export {Admin};