import {actionTypes} from './actionTypes';

const initialState = {
    messageList: [],
    editingMessageId: '',
};

export const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FILL_MESSAGES:
            return {
                ...state,
                messageList: [...action.payload]
            }

        case actionTypes.CREATE_MESSAGE:
            return {
                ...state,
                messageList: [...state.messageList, action.payload]
            }

        case actionTypes.TOGGLE_MESSAGE_LIKE:
            return {
                ...state,
                messageList: state.messageList.map(message => message.id !== action.payload
                    ? message
                    : {
                        ...message,
                        isLiked: !message.isLiked
                    }
                )
            }

        case actionTypes.DELETE_MESSAGE:
            return {
                ...state,
                messageList: state.messageList.filter(message => message.id !== action.payload)
            }

        case actionTypes.UPDATE_MESSAGE:
            return {
                ...state,
                messageList: state.messageList.map(post => post.id !== action.payload.id
                    ? post
                    : {
                        ...post,
                        message: action.payload.text
                    }
                )
            }

        case actionTypes.SET_EDITING_MESSAGE_ID:
            return {
                ...state,
                editingMessageId: action.payload
            }

        default:
            return state;
    }
}