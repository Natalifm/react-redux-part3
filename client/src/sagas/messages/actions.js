import {actionTypes} from './actionTypes';

export const messagesActions = {
    fetchMessages: () => ({
        type: actionTypes.FETCH_MESSAGES
    }),

    fillMessages: (messages) => ({
        type: actionTypes.FILL_MESSAGES,
        payload: messages
    }),

    createMessageAsync: (message) => ({
        type: actionTypes.CREATE_MESSAGE_ASYNC,
        payload: message
    }),

    createMessage: (message) => ({
        type: actionTypes.CREATE_MESSAGE,
        payload: message
    }),

    updateMessageAsync: (id, text) => ({
        type: actionTypes.UPDATE_MESSAGE_ASYNC,
        payload: {id, text}
    }),

    updateMessage: ({id, text}) => ({
        type: actionTypes.UPDATE_MESSAGE,
        payload: {id, text}
    }),

    deleteMessageAsync: (id) => ({
        type: actionTypes.DELETE_MESSAGE_ASYNC,
        payload: id
    }),

    deleteMessage: (id) => ({
        type: actionTypes.DELETE_MESSAGE,
        payload: id
    }),

    setEditingMessageId: (id) => ({
        type: actionTypes.SET_EDITING_MESSAGE_ID,
        payload: id
    }),

    toggleMessageLike: (id) => ({
        type: actionTypes.TOGGLE_MESSAGE_LIKE,
        payload: id
    }),
};