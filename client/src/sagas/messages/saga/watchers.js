import {takeEvery, all, call} from 'redux-saga/effects';
import {actionTypes} from '../actionTypes';
import {fetchMessages, createMessage, updateMessage, deleteMessage} from './workers';

function* watchFetchMessages() {
    yield takeEvery(actionTypes.FETCH_MESSAGES, fetchMessages);
}

function* watchCreateMessage() {
    yield takeEvery(actionTypes.CREATE_MESSAGE_ASYNC, createMessage);
}

function* watchUpdateMessage() {
    yield takeEvery(actionTypes.UPDATE_MESSAGE_ASYNC, updateMessage);
}

function* watchDeleteMessage() {
    yield takeEvery(actionTypes.DELETE_MESSAGE_ASYNC, deleteMessage);
}

export function* watchMessages() {
    yield all([
        call(watchFetchMessages),
        call(watchCreateMessage),
        call(watchUpdateMessage),
        call(watchDeleteMessage)
    ]);
}