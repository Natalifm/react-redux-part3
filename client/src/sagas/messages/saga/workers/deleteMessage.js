import {api} from '../../../../service';
import {messagesActions} from '../../actions';
import {uiActions} from '../../../ui/actions';
import {put, apply} from 'redux-saga/effects';

export function* deleteMessage({payload: id}) {
    try {
        yield put(uiActions.startFetching());
        const response = yield apply(api, api.messages.deleteMessage, [id]);
        if (response.status !== 200) {
            throw new Error();
        }
        yield put(messagesActions.deleteMessage(id));
    } catch (error) {
        console.error(error.message, 'deleteMessage worker');
    } finally {
        yield put(uiActions.stopFetching());
    }
}
