import {actionTypes} from './actionTypes';

const initialState = {
    isAuthenticated: false,
    isAdmin: false
};

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FILL_PROFILE:
            return {
                ...action.payload
            };

        default:
            return state;
    }
}