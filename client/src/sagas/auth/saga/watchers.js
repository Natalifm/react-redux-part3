import {takeEvery, all, call} from 'redux-saga/effects';
import {actionTypes} from '../actionTypes';
import {login} from './workers';

function* watchLogin() {
    yield takeEvery(actionTypes.LOGIN_ASYNC, login);
}

export function* watchAuth() {
    yield all([call(watchLogin)]);
}