import {actionTypes} from './actionTypes';

export const authActions = {
    loginAsync: (userCredentials) => ({
        type: actionTypes.LOGIN_ASYNC,
        payload: userCredentials
    }),

    fillProfile: (permissions) => ({
        type: actionTypes.FILL_PROFILE,
        payload: permissions
    }),
};