import {actionTypes} from './actionTypes';

export const userActions = {
    fetchUsers: () => ({
        type: actionTypes.FETCH_USERS
    }),

    fillUsers: (users) => ({
        type: actionTypes.FILL_USERS,
        payload: users
    }),

    createUserAsync: (user) => ({
        type: actionTypes.CREATE_USER_ASYNC,
        payload: user
    }),

    createUser: (user) => ({
        type: actionTypes.CREATE_USER,
        payload: user
    }),

    updateUserAsync: (user) => ({
        type: actionTypes.UPDATE_USER_ASYNC,
        payload: user
    }),

    updateUser: (user) => ({
        type: actionTypes.UPDATE_USER,
        payload: user
    }),

    deleteUserAsync: (id) => ({
        type: actionTypes.DELETE_USER_ASYNC,
        payload: id
    }),

    deleteUser: (id) => ({
        type: actionTypes.DELETE_USER,
        payload: id
    }),

    setEditingUserId: (id) => ({
        type: actionTypes.SET_EDITING_USER_ID,
        payload: id
    }),

    clearEditingUserId: () => ({
        type: actionTypes.CLEAR_EDITING_USER_ID
    }),
};