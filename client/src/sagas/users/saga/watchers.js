import {takeEvery, all, call} from 'redux-saga/effects';
import {actionTypes} from '../actionTypes';
import {fetchUsers, createUser, updateUser, deleteUser} from './workers';

function* watchFetchUsers() {
    yield takeEvery(actionTypes.FETCH_USERS, fetchUsers);
}

function* watchCreateUser() {
    yield takeEvery(actionTypes.CREATE_USER_ASYNC, createUser);
}

function* watchUpdateUser() {
    yield takeEvery(actionTypes.UPDATE_USER_ASYNC, updateUser);
}

function* watchDeleteUser() {
    yield takeEvery(actionTypes.DELETE_USER_ASYNC, deleteUser);
}

export function* watchUsers() {
    yield all([
        call(watchFetchUsers),
        call(watchCreateUser),
        call(watchUpdateUser),
        call(watchDeleteUser)
    ]);
}