import {actionTypes} from './actionTypes';

const initialState = {
    userList: [],
    editingUserId: ''
};

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FILL_USERS:
            return {
                ...state,
                userList: [...action.payload]
            }

        case actionTypes.CREATE_USER:
            return {
                ...state,
                userList: [...state.userList, action.payload]
            }

        case actionTypes.DELETE_USER:
            return {
                ...state,
                userList: state.userList.filter(user => user.id !== action.payload)
            }

        case actionTypes.UPDATE_USER:
            return {
                ...state,
                userList: state.userList.map(user => user.id !== action.payload.id
                    ? user
                    : {
                        ...action.payload
                    }
                )
            }

        case actionTypes.SET_EDITING_USER_ID:
            return {
                ...state,
                editingUserId: action.payload,
            }

        case actionTypes.CLEAR_EDITING_USER_ID:
            return {
                ...state,
                editingUserId: ''
            }

        default:
            return state;
    }
}