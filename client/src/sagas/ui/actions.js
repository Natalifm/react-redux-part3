import {actionTypes} from './actionTypes';

export const uiActions = {
    startFetching: () => ({
        type: actionTypes.START_FETCHING
    }),

    stopFetching: () => ({
        type: actionTypes.STOP_FETCHING
    }),
};
