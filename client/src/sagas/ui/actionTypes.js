export const actionTypes = {
    START_FETCHING: 'START_FETCHING',
    STOP_FETCHING: 'STOP_FETCHING'
}