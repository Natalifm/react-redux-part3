import {actionTypes} from './actionTypes';

const initialState = {
    isFetching: false
};

export const uiReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.START_FETCHING:
            return {
                ...state,
                isFetching: true
            }

        case actionTypes.STOP_FETCHING:
            return {
                ...state,
                isFetching: false
            }

        default:
            return state;
    }
};