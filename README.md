
## In the project directory:

1. Go to server directory `cd server`
2. `/server npm install`
3. `/server npm start`
4. Go to client directory `cd client`
5. `/client npm install`
6. `npm start`

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

1. For log in to chat as an admin: 
* `username: admin`
* `password: admin`
   
2. For log in to chat as a user:
* `enter a username: test`
* `password: test`

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
